Event State Machine
-------------------

Introduction and Purpose
~~~~~~~~~~~~~~~~~~~~~~~~

Within palaestrAI, many processes are based on events and transitions between
states. The Event State Machine (ESM) is a utility class that helps to
handle all event-based state transitions within palaestrAI. It
transparently handles the most common use cases:

  * dealing with ZMQ messages
  * monitoring processes
  * reacting to signals
  * handling exceptions that rise (from user code).

The ESM works by decorating the target class (called the *monitored class*).
It also injects some methods/properties into the target class. However, the
target class' code does not need to interact with the ESM; it can remain as-is,
without being subjected to a certain programming paradigm.

Class Documentation
~~~~~~~~~~~~~~~~~~~

.. autoclass:: palaestrai.core.EventStateMachine