"""

"""

from palaestrai.cli.manager import cli


def test_cli_commands_exist():
    tool = cli
    for cmd in ["experiment-start", "database-create", "database-migrate"]:
        assert cmd in tool.commands
